@extends('Layout.layout')
@section('content')
<div class="container">
        <a href="/addbanner"class="btn btn-primary" >Add New User</a>
    <h1>The List of Users</h1>
    <hr>
@csrf
@if ($users->isNotEmpty())
    
<table class="table table-sm" border="1">
    <thead>
        <tr>
            <th>SN</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Designation</th>
            <th>Address</th>
            <th>Image</th>
            <th>Operations</th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->gender}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->designation}}</td>
            <td>{{$user->address}}</td>
            <td><img src="{{asset('images/UserImages/'.$user->profile_image)}}" height="70px" width="70px"></td>
            <td>
                <button class="btn btn-secondary" type="edit" name="useredit"><a href={{'/useredit/'.$user->id}}>Edit</a></button>
                <button class="btn btn-danger"type="delete"name="userdelete"><a href={{'/userdelete/'.$user->id}}>Delete</a></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
@if ($users->isEmpty())
    <p>The table is empty.</p>
@endif
</div>
@endsection
@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="/index">Back to index</a>
    <h1>Edit User</h1>
    <hr>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
<form action=" {{url('updateuser/'.$user->id)}}" method="post" enctype="multipart/form-data">
@csrf
    <div>
        <label>FullName</label>
        <input type="text" class="form-control" name="name">
    </div>
    <div>
        <label>Profile Picture</label>
        <input type="file" class="form-control" name="profile_image">
    </div>
    <div>
        <label>Gender</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="others" name="gender" value="others">
        <label for="javascript">Others</label>
    </div>
    <div>
        <label>Email</label>
        <input type="email" class="form-control" name="email">
    </div>
    <div>
        <label>Designation</label>
        <input type="text" class="form-control" name="designation">
    </div>
    <div>
        <label>Address</label>
        <input type="text" class="form-control" name="address">
    </div>
    <div>
        <label>Password</label>
        <input type="password" class="form-control" name="password">
    </div>
    <div>
        <input type="submit" class="form-control">
    </div>
</form>
</div>
@endsection
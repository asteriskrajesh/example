@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="/index">Back to index</a>
    <h1>Add New Task</h1>
    <hr>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
<form action="/store" method="post"enctype="multipart/form-data">
@csrf
<h2><a href="/index">Back</a>
    <div>
        <label>Title:</label>
        <input type="text" class="form-control" name="title">
    </div>
    <div>
        <label>description:</label>
        <input type="text" class="form-control" name="description">
    </div>
    <div>
        <input type="submit" class="form-control">
    </div>
</form>
</div>
@endsection
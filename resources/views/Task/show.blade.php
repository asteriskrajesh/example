@extends('Layout.layout')
@section('content')
<div class="container">
        <a href="/addtask" class="btn btn-primary" >Add New Task</a>
    <h1>The List of Task</h1>
    <hr>
@csrf
@if ($tasks->isNotEmpty())
<table class="table table-sm" border="1">
    <thead>
        <tr>
            <th>SN</th>
            <th>Task Title</th>
            <th>Description</th>
            <th>Operation</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tasks as $task)
        <tr>
            <td>{{$task->id}}</td>
            <td>{{$task->title}}</td>
            <td>{{$task->description}}</td>
            <td>
                <button class="btn btn-secondary" type="edit" name="edit"><a href={{url('edit/'.$task->id)}}>Edit</a></button>
                <button class="btn btn-danger"type="delete"name="delete"><a href={{url('delete/'.$task->id)}}>Delete</a></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>    
@endif
@if ($tasks->isEmpty())
    <p>The table is empty</p>
@endif
</div>
@endsection
@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="/index">Back to Index</a>
    <a href="/addtask">Add Task</a>
    <a href="/show">View Task List</a>
</div>
@endsection
@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="/index">Back to index</a>
    <h1>Edit Task</h1>
    <hr>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
<form action="{{url('update/'.$tasks->id)}}" method="POST">
@csrf
<h2><a href="/show">Back</a>
    <div>
        <label>Task Id:{{$tasks->id}}</label>
    </div>
    <div>
        <label>Task Title:</label>
        <input type="text" value="{{$tasks->title}}"class="form-control" id="taskTitle" name="title">
    </div>
    <div>
        <label>Task Description:</label>
        <input type="text" value="{{$tasks->description}}" class="form-control" id="taskDescription" name="description">
    </div>
    <div>
        <input type="submit" class="btn btn-primary">
    </div>
</form>
</div>
@endsection
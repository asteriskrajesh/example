@extends('Layout.layout')
@section('content')
<div class="container">
<a class="btn btn-primary" href="/index">Back to index</a>
<h1>Add New Product</h1>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
<form action=" {{url('storeproduct')}}" method="post" enctype="multipart/form-data">
@csrf
<ul class="errorMessages"></ul>
    <div>
        <label>Name of Product</label>
        <input type="text" class="form-control" name="name">
    </div>
    <div>
        <label>Slug of Product</label>
        <input type="text" class="form-control" name="slug">
    </div>

    <div>
        <label>Price</label>
        <input type="float" class="form-control" name="price">
    </div>
    <div>
        <label>Image of product</label>
        <input type="file" class="form-control" name="Image">
    </div>
    <div>
        <label>Description</label>
        <input type="text" class="form-control" name="description">
    </div>
    <div>
        <input type="submit" class="form-control">
    </div>
</form>
</div>
@endsection
@extends('Layout.layout')
@section('content')
<div class="container">
<a class="btn btn-primary" href="/index">Back to index</a>
<h1>Add New Product</h1>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
<form action=" {{url('updateproduct/'.$product->id)}}" method="post" enctype="multipart/form-data">
@csrf
    <div>
        <label>Name of Product</label>
        <input type="text" class="form-control" name="name" value="{{$product->name}}">
    </div>
    <div>
        <label>Slug of Product</label>
        <input type="text" class="form-control" name="slug" value="{{$product->slug}}">
    </div>

    <div>
        <label>Price</label>
        <input type="float" class="form-control" name="price" value="{{$product->price}}">
    </div>
    <div>
        <label>Image of product</label>
        <input type="file" class="form-control" name="Image"value="{{$product->Image}}">
    </div>
    <div>
        <label>Description</label>
        <input type="text" class="form-control" name="description" value="{{$product->description}}">
    </div>
    <div>
        <input type="submit" class="form-control">
    </div>
</form>
</div>
@endsection
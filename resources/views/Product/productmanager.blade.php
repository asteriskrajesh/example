@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="/index">Back to Index</a>
    <a href="/addproduct">Add Product</a>
    <a href="/viewproduct">View Product List</a>
</div>
@endsection
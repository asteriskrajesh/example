@extends('Layout.layout')
@section('content')
<div class="container">    
    <a href="/addbanner" class="btn btn-primary" >Add New Product</a>
    <h1>The List of Product</h1>
    <hr>
@csrf
@if ($products->isNotEmpty)
<table class="table table-sm" border="1">
    <thead>
        <tr>
            <th>SN</th>
            <th>Product Name</th>
            <th>Slug of Product</th>
            <th>Status</th>
            <th>Price</th>
            <th>Image</th>
            <th>Description</th>
            <th>Operations</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->slug}}</td>
            <td>{{$product->status}}</td>
            <td>{{$product->price}}</td>
            <td><img src="{{asset('images/ProductImages/')}}/{{$product->Image}}" height="70px" width="70px"></td>
            <td>{{$product->description}}</td>
            <td>
                <button class="btn btn-secondary" type="edit" name="productedit"><a href={{'/productedit/'.$product->id}}>Edit</a></button>
                <button class="btn btn-danger"type="delete"name="productdelete"><a href={{'/productdelete/'.$product->id}}>Delete</a></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>    
@endif
@if ($products->isEmpty)
    <p>The table is empty</p>
@endif
</div>
@endsection
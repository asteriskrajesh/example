<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task Management</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">My Taskbar</a>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="/">Home</a>
      <a class="nav-item nav-link" href="/usermanager">User Manager</a>
      <a class="nav-item nav-link" href="/taskmanager">Task Manager</a>
      <a class="nav-item nav-link" href="/productmanager">Product Manager</a>
      <a class="nav-item nav-link" href="/categorymanager">Category Manager</a>
      <a class="nav-item nav-link" href="/bannermanager">Banner Manager</a>
      <a class="nav-item nav-link" href="/testmanager">Test Manager</a>
      <a class="nav-item nav-link" href="/studentmanager">Student Manager</a>
    </div>
  </div>
</nav>
<div class="container">
    @yield('content')
</div>
</body>
</html>
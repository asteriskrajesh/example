@extends('Layout.layout')
@section('content')
    <div class="container">
            <a class="btn btn-primary" href="/index">Back to index</a>
            <h1>Edit Student Detail</h1>
            <hr>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif
        <form class="form-group" method="POST" action="{{url('updatestudent/'.$student->id)}}" enctype="multipart/form-data">
            @csrf
            <div class="form-control">
                <label>Student Name</label>
                <input type="text" name="name" id="name" value="{{$student->name}}">
            </div>
            <div class="form-control">
                <label>Course</label>
                <input type="text" name="course" id="course"value="{{$student->course}}">
            </div>
            <div class="form-control">
                <label>Course Fee</label>
                <input type="number" name="fee" id="fee" value="{{$student->fee}}">
            </div>
            <div class="form-button">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
    </div>
@endsection
@extends('Layout.layout')
@section('content')
    <div class="container">
        <a class="btn btn-primary" href="/index">Back to index</a>
    <h1>List of Student</h1>
        @if ($students->isNotEmpty())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>SN</td>
                    <td>Student Name</td>
                    <td>Course</td>
                    <td>Course Fee</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($students as $student)
                <tr>
                    <td>{{$student->id}}</td>
                    <td>{{$student->name}}</td>
                    <td>{{$student->course}}</td>
                    <td>{{$student->fee}}</td>
                    <td>
                        <a class="btn btn-info" href="{{url('editstudent/'.$student->id)}}">Edit</a>
                        <a href="{{url('deletestudent/'.$student->id)}}"class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                
                @endforeach
            </tbody>
    </table>            
        @endif
        @if ($students->isEmpty())
           <p>The table is empty</p> 
        @endif
    </div>
@endsection
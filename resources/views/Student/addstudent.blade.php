@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-primary" href="/index">Back to index</a>
    <h1>Add Student Form</h1>
    <hr>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
        <form class="form-group" method="POST" action="{{url('storestudent')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-control">
                <label>Student Name</label>
                <input type="text" name="name" id="name">
            </div>
            <div class="form-control">
                <label>Course</label>
                <input type="text" name="course" id="course">
            </div>
            <div class="form-control">
                <label>Course Fee</label>
                <input type="number" name="fee" id="fee">
            </div>
            <div class="form-button">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
    </div>
@endsection
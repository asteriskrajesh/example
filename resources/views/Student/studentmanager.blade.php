@extends('Layout.layout')
@section('content')
    <div class="container">
        <a class="btn btn-primary" href="/index">Back</a>
        <a class="btn btn-info" href="/addstudent">Add Student</a>
        <a class="btn btn-danger" href="/liststudent">Student List</a>
    </div>
@endsection
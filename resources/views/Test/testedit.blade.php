@extends('Layout.layout')
@section('content')
    <div class="container">
            <a class="btn btn-primary" href="/index">Back to index</a>
            <h1>Edit Test</h1>
            <hr>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif
        <form action="{{url('testupdate/'.$tests->id)}} " method="POST"enctype="multipart/form-data">
            @csrf
            <div>
                <label>Test Id:{{$tests->id}}</label>
            </div>
            <div>
                <label>Test Name</label>
                <input type="text" value="{{$tests->name}}" class="form-control" id="name" name="name">
            </div>
            <div>
                <label>Test Type</label>
                <input type="text" value="{{$tests->type}}" class="form-control" id="type" name="type">
            </div>
            <div>
                <label>Test Image</label>
                <input type="file" value="{{$tests->image}}" class="form-control" id="image" name="image">
            </div>
            <div>
                <label>Test Date</label>
                <input type="date" value="{{$tests->mydate}}" class="form-control" id="mydate" name="mydate">
            </div>
            <div>
                <input type="submit" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection
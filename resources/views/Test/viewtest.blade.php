@extends('Layout.layout')
@section('content')
    <div class="container">
        <a class="btn btn-danger" href="/createtest">Add new test</a>
        <h1>Test List</h1>
        <hr>
        @csrf
        @if ($tests->isNotEmpty())
        <table class="table table-sm" border="1">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Test Name</th>
                    <th>Test Type</th>
                    <th>Test Image</th>
                    <th>Test Date</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tests as $test)
                    <tr>
                        <td>{{$test->id}}</td>
                        <td>{{$test->name}}</td>
                        <td>{{$test->type}}</td>
                        <td><img src="{{asset('images/TestImages/'.$test->image)}}" height="70px" width="70px"></td>
                        <td>{{$test->mydate}}</td>
                        <td>
                            <button class="btn btn-info" type="edit" name="testedit"><a href={{url('testedit/'.$test->id)}}>Edit</a></button>
                            <button class="btn btn-danger" type="delete" name="testdelete"><a href={{url('testdelete/'.$test->id)}}>Delete</a></button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>            
        @endif
        @if ($tests->isEmpty())
            <p>The table is Empty.</p>
        @endif
    </div>
@endsection
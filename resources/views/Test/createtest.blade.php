@extends('Layout.layout')
@section('content')
    <div class="container">
        <a class="btn btn-primary" href="/index">Back to index</a>
        <h1>Create Test Page</h1>
    <hr>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
        <div class="container-primary">
            <form class="form-group" action="/storetest" method="post" enctype="multipart/form-data">
            @csrf
                <div>
                    <label>Test Name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div>
                    <label>Test Type</label>
                    <input type="text" class="form-control" name="type">
                </div>
                <div>
                    <label>Test Image</label>
                    <input type="file" class="form-control" name="image">
                </div>
                <div>
                    <label>Test Date</label>
                    <input type="date" class="form-control" name="mydate">
                </div>
                <div>
                    <input type="submit" class="form-control">
                </div>
            </form>  
        </div>
    </div>
@endsection
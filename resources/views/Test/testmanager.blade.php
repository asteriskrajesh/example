@extends('Layout.layout')
@section('content')
<div class="container">
    <a class="btn btn-danger" href="/index">Back to index</a>
    <a class="btn btn-primary" href="/createtest">Add Test</a>
    <a class="btn btn-info" href="/viewtest">View Test</a>
</div>
@endsection
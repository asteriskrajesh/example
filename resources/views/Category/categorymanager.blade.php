@extends('Layout.layout')
@section('content')
<div class="container">
  <a class="btn btn-primary" href="/index">Back to Index</a>
  <a href="/addcategory">Add Category</a>
  <a href="/viewcategory">View Category List</a>
</div>
@endsection
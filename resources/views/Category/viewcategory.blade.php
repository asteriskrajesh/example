@extends('Layout.layout')
@section('content')
<div class="container">    
    <a href="/addbanner" class="btn btn-primary" >Add New Category</a>
    <div>
    <h1>The List of Category</h1>
    <hr>
    @csrf
    @if ($categories->isNotEmpty())
    <table class="table table-sm" border="1">
    <thead>
        <tr>
            <th>SN</th>
            <th>Category Name</th>
            <th>Slug</th>
            <th>Status</th>
            <th>Image</th>
            <th>Operations</th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{$category->id}}</td>
            <td>{{$category->name}}</td>
            <td>{{$category->slug}}</td>
            <td>{{$category->status}}</td>
            <td><img src="{{asset('images/CategoryImages/')}}/{{$category->image}}" height="70px" width="70px"></td>
            <td>
                <button class="btn btn-secondary" type="edit" name="categoryedit"><a href={{'/categoryedit/'.$category->id}}>Edit</a></button>
                <button class="btn btn-danger"type="delete"name="categorydelete"><a href={{'/bannerdelete/'.$category->id}}>Delete</a></button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif
@if ($categories->isEmpty())
The table is empty.
@endif
    </div>
@endsection
@extends('Layout.layout')
@section('content')
<div class="container">
<a class="btn btn-primary" href="/index">Back to index</a>
<h1>Edit Category</h1>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
<form action=" {{url('updatecategory/'.$category->id)}}" method="post" enctype="multipart/form-data">
@csrf
    <ul class="errorMessages"></ul>
    <div>
        <label>Name of Category:</label>
        <input type="text" class="form-control" name="name" value="{{$category->name}}">
    </div>
    <div>
        <label>Image of Category</label>
        <input type="file" class="form-control" name="image" value="{{$category->image}}">
    </div>
    <div>
        <label>Start Date</label>
        <input type="text" class="form-control" name="start_date" value="{{$category->start_date}}">
    </div>
    <div>
        <input type="submit" class="form-control">
    </div>
</form>
</div>
@endsection
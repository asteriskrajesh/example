@extends('Layout.layout')
@section('content')
<div class="container"
    <div class="jumbotron text-center">    
            <a class="btn btn-info" href="/addbanner">Add New Banner</a>
    </div>
    <h1>The List of Banner</h1>
    <hr>
    @csrf
    @if ($banners->isNotEmpty())
    <table class="table table-sm" border="1">
        <thead>
            <tr>
                <th>SN</th>
                <th>Banner Name</th>
                <th>Banner Start Date</th>
                <th>Banner End Date</th>
                <th>Banner Image</th>
                <th>Banner Image Url</th>
                <th>Operations</th> 
            </tr>
        </thead>
        <tbody>
            @foreach($banners as $banner)
            <tr>
                <td>{{$banner->id}}</td>
                <td>{{$banner->name}}</td>
                <td>{{$banner->start_date}}</td>
                <td>{{$banner->end_date}}</td>
                <td><img src="{{asset('images/BannerImages/')}}/{{$banner->Image}}" height="70px" width="70px"></td>
                <td>{{$banner->url}}</td>
                <td>
                    <button class="btn btn-secondary" type="edit" name="banneredit"><a href={{'/banneredit/'.$banner->id}}>Edit</a></button>
                    <button class="btn btn-danger"type="delete"name="bannerdelete"><a href={{'/bannerdelete/'.$banner->id}}>Delete</a></button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>        
    @endif
    @if ($banners->isEmpty())
    <div>
        The table is empty.
    </div>
    @endif

</div>
@endsection
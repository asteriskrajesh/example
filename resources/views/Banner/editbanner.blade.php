@extends('Layout.layout')
@section('content')
<a class="btn btn-primary" href="/index">Back to index</a>
<h1>Edit Banner</h1>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
<form action=" {{url('updatebanner/'.$banner->id)}}" method="post" enctype="multipart/form-data">
@csrf
    <ul class="errorMessages"></ul>
    <div>
        <label>Name of banner:</label>
        <input type="text" class="form-control" name="name"value="{{$banner->name}}">
    </div>
    <div>
        <label>Image of Banner:</label>
        <input type="file" class="form-control" name="Image" value="{{$banner->Image}}">
    </div>
    <div>
        <label>Start Date:</label>
        <input type="date" class="form-control" name="start_date"value="{{$banner->start_date}}">
    </div>
    <div>
        <label>End Date:</label>
        <input type="date" class="form-control" name="end_date" value="{{$banner->end_date}}">
    </div>
    <div>
        <input type="submit" class="form-control">
    </div>
</form>
@endsection
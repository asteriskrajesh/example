<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[TasksController::class, 'index']);
Route::get('/taskmanager',[HomeController::class, 'taskmanager']);
Route::get('/index',[TasksController::class, 'index']);
Route::get('/addtask',[TasksController::class, 'create']);
Route::post('/store',[TasksController::class,'store']);
Route::get('/show',[TasksController::class,'show']);
Route::get('/edit/{id}',[TasksController::class,'edit']);
Route::post('/update/{id}',[TasksController::class,'update']);
Route::get('/delete/{id}',[TasksController::class,'destroy']);


Route::get('/productmanager',[HomeController::class, 'productmanager']);
Route::get('/addproduct',[ProductController::class,'addproduct']);
Route::get('/viewproduct',[ProductController::class,'viewproduct']);
Route::post('/storeproduct',[ProductController::class,'storeproduct']);
Route::get('/editproduct/{id}',[ProductController::class,'editproduct']);
Route::post('/updateproduct/{id}',[ProductController::class,'updateproduct']);
Route::get('/deleteproduct/{id}',[ProductController::class,'destroyproduct']);


Route::get('/categorymanager',[HomeController::class, 'categorymanager']);
Route::get('/addcategory',[CategoryController::class,'addcategory']);
Route::get('/viewcategory',[CategoryController::class,'viewcategory']);
Route::post('/storecategory',[CategoryController::class,'storecategory']);
Route::get('/editcategory/{id}',[CategoryController::class,'editcategory']);
Route::post('/updatecategory/{id}',[CategoryController::class,'updatecategory']);
Route::get('/deletecategory/{id}',[CategoryController::class,'destroycategory']);

Route::get('/bannermanager',[HomeController::class, 'bannermanager']);
Route::get('/addbanner',[BannerController::class,'addbanner']);
Route::get('/viewbanner',[BannerController::class,'viewbanner']);
Route::post('/storebanner',[BannerController::class,'storebanner']);
Route::get('/editbanner/{id}',[BannerController::class,'editbanner']);
Route::post('/updatebanner/{id}',[BannerController::class,'updatebanner']);
Route::get('/deletebanner/{id}',[BannerController::class,'destroybanner']);

Route::get('/usermanager',[HomeController::class, 'usermanager']);
Route::get('/adduser',[UserController::class,'adduser']);
Route::get('/listuser',[UserController::class,'listuser']);
Route::post('/storeuser',[UserController::class,'storeuser']);
Route::get('/edituser/{id}',[UserController::class,'edituser']);
Route::post('/updateuser/{id}',[UserController::class,'updateuser']);
Route::get('/deleteuser/{id}',[UserController::class,'destroyuser']);

Route::get('/testmanager',[HomeController::class,'testmanager']);
Route::get('/createtest',[TestController::class,'createtest']);
Route::post('/storetest',[TestController::class,'storetest']);
Route::get('/viewtest',[TestController::class,'viewtest']);
Route::get('/testedit/{id}',[TestController::class,'testedit']);
Route::post('/testupdate/{id}',[TestController::class,'testupdate']);
Route::get('/testdelete/{id}',[TestController::class,'testdelete']);

Route::get('/studentmanager',[HomeController::class,'studentmanager']);
Route::get('/addstudent',[StudentsController::class,'addstudent']);
Route::post('/storestudent',[StudentsController::class,'storestudent']);
Route::get('/liststudent',[StudentsController::class,'liststudent']);
Route::get('/editstudent/{id}',[StudentsController::class,'editstudent']);
Route::post('/updatestudent/{id}',[StudentsController::class,'updatestudent']);
Route::get('/deletestudent/{id}',[StudentsController::class,'deletestudent']);

<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function viewproduct(){
        $products=Products::latest()->paginate(10);
        return view('Product.viewproduct',compact('products'));
    }
    public function addproduct(){
        return view('Product.addproduct');
    }
    public function storeproduct(Request $request){
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'Image'=>'required',
        ]);
        $data=new Products();
        $data->name=$request->name;
        $data->slug=$request->slug;
        $data->price=$request->price;

        // dd($data);
        $Imagename=time().'.'.$request->Image->extension();
        $request->Image->move(public_path('images/ProductImages'),$Imagename);
        $data->Image=$Imagename;
        $data->status=true;
        $data->description=$request->description;
        $data->save();

        return redirect('/viewproduct');
    }
    public function editproduct($id){
        $product=Products::find($id);
        return view('editproduct',compact('product'));
    }
    public function updateproduct(Request $request,$id){
        $product=Products::find($id);
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'Image'=>'required'
        ]);
        $product->name=$request->name;
        $product->slug=$request->slug;
        $product->price=$request->price;
        $Imagename=time().'.'.$request->Image->extension();
        $request->Image->move(public_path('images/ProductImages'),$Imagename);
        $product->Image=$Imagename;
        $product->status=true;
        $product->description=$request->description;
        $product->save();

        return redirect('/viewproduct');
    }
    public function deleteproduct($id){
        $product=Products::find($id);
        $product->delete();
        return redirect('/viewproduct');
    }
}

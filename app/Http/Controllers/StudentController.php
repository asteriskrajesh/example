<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use PHPUnit\Framework\MockObject\Builder\Stub;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function liststudent()
    {
        //
        $students=Student::all();
        
        return view('Student.liststudent',compact('students'));
        // ->with('i',(request()->input('page',1)-1)*5));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addstudent()
    {
        return view('Student.addstudent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storestudent(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'course'=>'required',
            'fee'=>'required'
        ]);
        $student=new Student();
        $student->name=$request->name;
        $student->course=$request->course;
        $student->fee=$request->fee;
        $student->save();
        return redirect('/liststudent')->with('success','Student added successfully');        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function editstudent($id)
    {
        $student=Student::find($id);
        return view('Student.editstudent',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function updatestudent(Request $request,$id)
    {
        //
        $request->validate([
            'name'=>'required',
            'course'=>'required',
            'fee'=>'required'
        ]);
        $student=Student::find($id);
        $student->name=$request->name;
        $student->course=$request->course;
        $student->fee=$request->fee;
        $student->save();
        return redirect('/liststudent')->with('success','Student updated successfully');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function deletestudent($id)
    {
        //
        $student=Student::find($id);
        $student->delete();
        return redirect('/liststudent')
        ->with('success','Student deleted successfully');
    }
}

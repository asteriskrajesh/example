<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function listuser(User $users)
    {
        $users=User::all();
        return view('User.listuser',compact('users'));
    }
    public function adduser(){
        return view('User.adduser');
    }
    public function storeuser(Request $request){
        $request->validate([
            'name'=>'required',
            'gender'=>'required',
            'email'=>'required',
            'password'=>'required',
            'address'=>'required',
            // 'profile_image'=>'required',
        ]);
        $data=new User();
        $data->name=$request->name;
        $data->gender=$request->gender;
        $data->email=$request->email;
        $data->password=$request->password;
        $data->address=$request->address;
        $data->designation=$request->designation;
        // dd($data);
        $Imagename=time().'.'.$request->profile_image->extension();
        $request->profile_image->move(public_path('images/UserImages'),$Imagename);
        $data->profile_image=$Imagename;
        $data->save();

        return redirect('/listuser');
    }
    public function edituser($id){
        $user=User::find($id);
        return view('edituser',compact('user'));
    }
    public function updateuser(Request $request,$id){
        $user=User::find($id);
        $request->validate([
            'name'=>'required',
            'address'=>'required',
            'email'=>'required',
            'password'=>'required',
            'gender'=>'required'
        ]);
        $user->name=$request->name;
        $user->gender=$request->gender;
        $user->email=$request->email;
        $user->password=$request->password;
        $user->address=$request->address;
        $user->designation=$request->designation;
        $myImage=time().'.'.$request->profile_image->extension();
        $request->profile_image->move(public_path('images/UserImages'),$myImage);
        $user->profile_image=$myImage;
        $user->save();
        return redirect('/listuser');
    }
    public function deleteuser($id){
        $user=User::find($id);
        $user->delete();
        return redirect('/listuser');
    }
}

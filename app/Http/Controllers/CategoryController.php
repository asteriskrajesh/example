<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function viewcategory(Categories $categories)
    {
        $categories=Categories::all();
        return view('Category.viewcategory',compact('categories'));
    }

    public function addcategory()
    {
        //
        return view('Category.addcategory');
    }
    public function storecategory(Request $request)
    {
        //
        $request->validate([
            'name'=>'required'
        ]);
        $data=new Categories();
        $data->name=$request->name;
        $data->slug=$request->slug;
        $data->status=$request->true;
        // dd($data);
        $Imagename=time().'.'.$request->image->extension();
        $request->image->move(public_path('images/CategoryImages'),$Imagename);
        $data->image=$Imagename;
        $data->save();

        return redirect('/viewcategory');
    }
    public function editcategory($id){
        $category=Categories::find($id);
        return view('editcategory',compact('category'));
    }
    public function updatecategory(Request $request,$id){
        $request->validate([
            'name'=>'required'
        ]);
        $category=Categories::find($id);
        $category->name=$request->name;
        $category->slug=$request->slug;
        $category->status=$request->status->true;
        $Imagename=time().'.'.$request->image->extension();
        $request->image->move(public_path('images/CategotyImages'),$Imagename);
        $category->image=$Imagename;
        $category->image=$Imagename;
        $category->save();
        return redirect('/viewcategory');
    }
    public function deletecategory($id){
        $category=Categories::find($id);
        $category->delete();
        return redirect('/viewcategory');
    }
}
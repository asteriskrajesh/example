<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //index page
    public function index()
    {
        //
        return view('Layout.index');
    }
    public function bannermanager(){
        return view('Banner.bannermanager');
    }
    public function taskmanager(){
        return view('Task.taskmanager');
    }
    public function usermanager(){
        return view('User.usermanager');
    }
    public function productmanager(){
        return view('Product.productmanager');
    }
    public function categorymanager(){
        return view('Category.categorymanager');
    }
    public function testmanager(){
        return view('Test.testmanager');
    }
    public function studentmanager(){
        return view('Student.studentmanager');
    }
}

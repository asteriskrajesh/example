<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Banners;

class BannerController extends Controller
{
    
    public function viewbanner(Banners $banners)
    {
        $banners=Banners::all();
        return view('Banner.viewbanner',compact('banners'));
    }

    public function addbanner()
    {
        //
        return view('Banner.addbanner');
    }
    public function storebanner(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'start_date'=>'required',
            'Image'=>'required',
            'url'=>'required',
        ]);
        $data=new Banners();
        $data->name=$request->name;
        $data->start_date=$request->start_date;
        $data->end_date=$request->end_date;
        // dd($data);
        $Imagename=time().'.'.$request->Image->extension();
        $request->Image->move(public_path('images/BannerImages'),$Imagename);
        $data->Image=$Imagename;
        $data->url='/'.$Imagename;
        $data->save();

        return redirect('/viewbanner');
    }
    public function editbanner($id){
        $banner=Banners::find($id);
        return view('Banner.editbanner',compact('banner'));
    }
    public function updatebanner(Request $request,$id){
        $request->validate([
            'name'=>'required',
            'start_date'=>'required',
            'Image'=>'required',
            'url'=>'required',
        ]);
        $banner=Banners::find($id);
        $banner->name=$request->name;
        $banner->start_date=$request->start_date;
        $banner->end_date=$request->end_date;
        // dd($data);
        $Imagename=time().'.'.$request->Image->extension();
        $request->Image->move(public_path('images/BannerImages'),$Imagename);
        $banner->Image=$Imagename;
        $banner->url='/'.$Imagename;
        $banner->save();
        return redirect('/viewbanner');
    }
    public function delete($id){
        $banner=Banners::find($id);
        $banner->delete();
        return redirect('/viewbanner');
    }
}

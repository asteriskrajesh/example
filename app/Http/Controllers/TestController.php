<?php

namespace App\Http\Controllers;

use App\Models\Tasks;
use App\Models\Test;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Tests;

class TestController extends Controller
{
    //
    public function createtest(){
        return view('Test.createtest');
    }
    public function storetest(Request $request){
        $request->validate([
            'name'=>'required',
            'type'=>'required',
            'image'=>'required',
            'mydate'=>'required',
        ]);
        $data=new Test();
        $data->name=$request->name;
        $data->type=$request->type;
        $Imagename=time().'.'.$request->image->extension();
        $request->image->move(public_path('images/TestImages'),$Imagename);
        $data->image=$Imagename;
        $data->mydate=$request->mydate;

        $data->save();
        return redirect('/viewtest');
    }
    public function viewtest(Test $tests){
        $tests=Test::all();
        // dd($tests);
        return view('Test.viewtest',compact('tests'));
    }

    public function testedit($id){
        $tests=Test::find($id);
        return view('Test.testedit',compact('tests'));
    }
    public function testupdate(Request $request,$id){
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'image' => 'required',
            'mydate' => 'required',
        ]);
       
        $tests=Test::find($id);
        // dd($tests);

        $tests->name=$request->name;
        $tests->type=$request->type;
        $tests->mydate=$request->mydate;
        $testimage=time().'.'.$request->image->extension();
        $request->image->move(public_path('images/TestImages'),$testimage);
        $tests->image=$testimage;
        $tests->save();
      
        // $tasks->update($request->all());
        // $task = Tasks::update(['id'=>$request->id,'title' => $request->title,'description' => $request->description]);
        // $allt=Tasks::all();
        return redirect('/viewtest')->with('success', 'Test updated successfully');

    }
    public function testdelete($id){
        
        $tests=Test::find($id)->delete();

        return redirect('/viewtest')->with('success', 'Test deleted successfully');
    }

}
